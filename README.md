# DWM-Config <br>
My DWM Config requires:<br>
dmenu<br>
ST<br>

My autostart requires:<br>
feh with Xinerama support enabled (use "man feh" to check)<br>
picom<br>
lxqt-notificationd<br>
lxqt-policykit<br>
volumeicon<br>
sxhkd<br>
xss-lock<br>
i3lock<br>
xset<br>
dwmblocks (included)<br>
mpv (for the startup sound)<br>
qtshutdownmenu<br>

I have included an installer for my dwm config.<br>
My installer assumes you git cloned my repo in the root of your home directory<br>
(~/dwmconfig).<br>

Besides the little documentation above. I won't provide further<br>
documentation. It's up the user (you) to make changes to what they<br>
(you) don't want.<br>
