# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
         # Shell is non-interactive.  Be done now!
         return
fi

export PS1="\[\e[36m\]\u\[\e[m\]\[\e[35m\]@\[\e[m\]\[\e[34m\]\h\[\e[m\]\[\e[32m\]\w\[\e[m\]\[\e[36m\] >\[\e[m\] "

# ls alias
alias la="ls -a"
alias ll="ls -la"

# Fun Zone

alias nclear="clear ; neofetch"
