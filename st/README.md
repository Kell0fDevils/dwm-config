# ST DistroTube Fork

Fork of DT's heavily-patched customized build of the Suckless simple terminal (st). <br>

 # Installing The Fork

* On Arch Linux: <br>
 makepkg -Si  <br>

* On other distros: <br>
 git clone https://gitlab.com/Kell0fDevils/st-distrotube-fork.git  <br>
 cd st-distrotube-fork <br>
sudo make clean install  <br>

=NOTE:= Installing st-distrotube will overwrite your existing st installation so make a<br> backup of your current config if you need it.


